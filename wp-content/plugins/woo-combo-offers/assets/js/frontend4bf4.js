jQuery( document ).ready( function( jQuery ) {
	wooco_check_variables();

	jQuery( '.product-type-wooco .wooco-products select' ).on( 'change', function() {
		jQuery( this ).closest( '.wooco-product' ).attr( 'data-id', 0 );
		wooco_check_variables();
	} );

	jQuery( document ).on( 'found_variation', function( e, t ) {
		if ( jQuery( '#wooco_products' ).length ) {
			if ( t['image']['url'] && t['image']['srcset'] ) {
				// change image
				jQuery( e['target'] ).closest( '.wooco-product' ).find( '.wooco-thumb-ori' ).hide();
				jQuery( e['target'] ).closest( '.wooco-product' ).find( '.wooco-thumb-new' ).html( '<img src="' + t['image']['url'] + '" srcset="' + t['image']['srcset'] + '"/>' ).show();
			}
			if ( t['price_html'] ) {
				// change price
				jQuery( e['target'] ).closest( '.wooco-product' ).find( '.wooco-price-ori' ).hide();
				jQuery( e['target'] ).closest( '.wooco-product' ).find( '.wooco-price-new' ).html( t['price_html'] ).show();
			}
			if ( t['is_purchasable'] ) {
				// change stock notice
				if ( t['is_in_stock'] ) {
					jQuery( '#wooco_wrap' ).next( 'p.stock' ).show();
					jQuery( e['target'] ).closest( '.wooco-product' ).attr( 'data-id', t['variation_id'] );
					jQuery( e['target'] ).closest( '.wooco-product' ).attr( 'data-price', t['display_price'] );
					wooco_check_variables();
					wooco_save_ids();
				} else {
					jQuery( '#wooco_wrap' ).next( 'p.stock' ).hide();
				}
				if ( t['availability_html'] != '' ) {
					jQuery( e['target'] ).closest( '.variations_form' ).find( 'p.stock' ).remove();
					jQuery( e['target'] ).closest( '.variations_form' ).append( t['availability_html'] );
				}
			}
			if ( t['variation_description'] != '' ) {
				jQuery( e['target'] ).closest( '.wooco-product' ).find( '.wooco-variation-description' ).html( t['variation_description'] ).show();
			} else {
				jQuery( e['target'] ).closest( '.wooco-product' ).find( '.wooco-variation-description' ).html( '' ).hide();
			}
		}
	} );

	jQuery( document ).on( 'reset_data', function( e ) {
		if ( jQuery( '#wooco_products' ).length ) {
			// reset thumb
			jQuery( e['target'] ).closest( '.wooco-product' ).find( '.wooco-thumb-new' ).hide();
			jQuery( e['target'] ).closest( '.wooco-product' ).find( '.wooco-thumb-ori' ).show();
			// reset price
			jQuery( e['target'] ).closest( '.wooco-product' ).find( '.wooco-price-new' ).hide();
			jQuery( e['target'] ).closest( '.wooco-product' ).find( '.wooco-price-ori' ).show();
			// reset stock
			jQuery( e['target'] ).closest( '.variations_form' ).find( 'p.stock' ).remove();
			// reset desc
			jQuery( e['target'] ).closest( '.wooco-product' ).find( '.wooco-variation-description' ).html( '' ).hide();
		}
	} );

	jQuery( '.single_add_to_cart_button' ).click( function() {
		jQuery.ajax( {
			url: wooco_vars.ajax_url,
			type: "POST",
			data: {
				action: 'wooco_custom_data',
				wooco_ids: jQuery( '#wooco_ids' ).val(),
				wooco_nonce: wooco_vars.wooco_nonce
			},
			async: false,
		} );
	} );

	jQuery( '.product-type-wooco' ).on( 'click', '.single_add_to_cart_button.disabled', function( e ) {
		e.preventDefault();
		alert( wooco_vars.alert_text );
	} );
} );

function wooco_check_variables() {
	if ( jQuery( '#wooco_products' ).attr( 'data-variables' ) == 'yes' ) {
		var is_ok = true;
		jQuery( '.product-type-wooco .wooco-products .wooco-product' ).each( function() {
			if ( jQuery( this ).attr( 'data-id' ) == 0 ) {
				is_ok = false;
				return;
			}
		} );
		if ( is_ok ) {
			jQuery( '.product-type-wooco .single_add_to_cart_button' ).removeClass( 'disabled' ).removeClass( 'wc-variation-selection-needed' );
			wooco_total_price();
		} else {
			jQuery( '.product-type-wooco .single_add_to_cart_button' ).addClass( 'disabled' ).addClass( 'wc-variation-selection-needed' );
			jQuery( '#wooco_total' ).slideUp();
		}
	}
}

function wooco_total_price() {
	var total = 0;
	var total_html = wooco_vars.bundle_price_text + ' ';
	jQuery( '.product-type-wooco .wooco-products .wooco-product' ).each( function() {
		if ( jQuery( this ).attr( 'data-price' ) > 0 ) {
			total += jQuery( this ).attr( 'data-price' ) * jQuery( this ).attr( 'data-qty' );
		}
	} );
	if ( (
		     jQuery( '#wooco_products' ).attr( 'data-percent' ) > 0
	     ) && (
		     jQuery( '#wooco_products' ).attr( 'data-percent' ) < 100
	     ) ) {
		total = (
			        total * jQuery( '#wooco_products' ).attr( 'data-percent' )
		        ) / 100;
	}
	else if ( jQuery( '#wooco_products' ).attr( 'data-sale' ) > 0 ) {
		total = jQuery( '#wooco_products' ).attr( 'data-sale' );
	}
	total = wooco_format_money( total, wooco_vars.price_decimals, '', wooco_vars.price_thousand_separator, wooco_vars.price_decimal_separator );
	switch ( wooco_vars.price_format ) {
		case '%1$s%2$s':
			//left
			total_html += wooco_vars.currency_symbol + '' + total;
			break;
		case '%1$s %2$s':
			//left with space
			total_html += wooco_vars.currency_symbol + ' ' + total;
			break;
		case '%2$s%1$s':
			//right
			total_html += total + '' + wooco_vars.currency_symbol;
			break;
		case '%2$s %1$s':
			//right with space
			total_html += total + ' ' + wooco_vars.currency_symbol;
			break;
		default:
			//default
			total_html += wooco_vars.currency_symbol + '' + total;
	}
	jQuery( '#wooco_total' ).html( total_html ).slideDown();
}

function wooco_save_ids() {
	var listId = Array();
	jQuery( '.product-type-wooco .wooco-products .wooco-product' ).each( function() {
		if ( jQuery( this ).attr( 'data-id' ) != 0 ) {
			listId.push( jQuery( this ).attr( 'data-id' ) + '/' + jQuery( this ).attr( 'data-qty' ) );
		}
	} );
	jQuery( '#wooco_ids' ).val( listId.join( ',' ) );
}

function wooco_format_money( number, places, symbol, thousand, decimal ) {
	number = number || 0;
	places = ! isNaN( places = Math.abs( places ) ) ? places : 2;
	symbol = symbol !== undefined ? symbol : "$";
	thousand = thousand || ",";
	decimal = decimal || ".";
	var negative = number < 0 ? "-" : "",
		i = parseInt( number = Math.abs( + number || 0 ).toFixed( places ), 10 ) + "",
		j = (
			    j = i.length
		    ) > 3 ? j % 3 : 0;
	return symbol + negative + (
			j ? i.substr( 0, j ) + thousand : ""
		) + i.substr( j ).replace( /(\d{3})(?=\d)/g, "$1" + thousand ) + (
		       places ? decimal + Math.abs( number - i ).toFixed( places ).slice( 2 ) : ""
	       );
}